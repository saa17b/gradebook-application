# C\# Fundamentals #

This course, C# Fundamentals, will help you be comfortable with fundamental programming concepts on any platform. First, you will learn about the syntax of the C# language. Next, you will discover the built-in features of .NET. Finally, you will explore how to solve problems using object-oriented programming techniques. When you are finished with this course, you will have the skills and knowledge you need for real-world solutions.
    - [Click here](https://www.pluralsight.com/courses/csharp-fundamentals-dev) to visit course.
        
